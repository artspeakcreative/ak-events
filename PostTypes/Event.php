<?php
namespace PostType;

/**
 * Event Post Type
 */
class Event extends BasePost
{
 const POST_TYPE = "Event";

 /**- - - - - - - - - - - - - -
  * CONSTRUCTOR
  * - - - - - - - - - - - - - - */
 public function __construct($post)
 {
  parent::__construct($post);

  // Start and End variables
  $this->display_start_date = $this->create_date($this->start_time);
  $this->display_start_time = $this->create_time($this->start_time);
  $this->start_date_time = new \DateTimeImmutable($this->start_time, wp_timezone());

  if ($this->end_time) {
   $this->display_end_date = $this->create_date($this->end_time);
   $this->display_end_time = $this->create_time($this->end_time);
   $this->end_date_time = new \DateTimeImmutable($this->end_time, wp_timezone());
  }

  $this->date = $this->start_date_time->format('Y-m-d');
  $this->info = array(
   'title' => $this->title,
   'start-date' => $this->start_date_time->format("M Y"),
  );

  // Expiration
  $this->expiration = $this->start_date_time;
  if ($this->end_time) {
   $this->expiration = $this->end_date_time;
  }
  $this->expire();

  // Directions
  if ($this->address) {
   $this->directions = "https://www.google.com/maps/dir//" . \urlencode($this->address);
  }

  // Primary Category
  $terms = wp_get_post_terms($this->ID, 'event_category');
  $this->categories = $terms;
  foreach ($terms as $term) {
   if (get_post_meta($this->ID, '_yoast_wpseo_primary_event_category', true) == $term->term_id) {
    $this->primary_category = $term->name;
   }
  }

  // Signup Link vs Form
  if ($this->sign_up) {
   $key = substr($this->sign_up, 0, 1);
   if ($key == '[') {
    // Assume shortcode
    $this->sign_up_form = $this->sign_up;
   } else {
    // Assume Link
    $this->sign_up_link = $this->sign_up;
   }
  }
 }

 /**- - - - - - - - - - - - - -
  * STATIC FUNCTIONS
  * - - - - - - - - - - - - - - */

 /**
  * Initial setup of all static functions
  *
  * @return void
  */
 public static function setup()
 {
  Event::register();
  Event::fields();
  Event::createRoutes();
  Event::setupSorting();
 }

 /**
  * Register the class in WP
  *
  * @return void
  */
 public static function register()
 {
  register_extended_post_type(self::POST_TYPE, array(
   'show_in_feed' => false,
   'menu_icon' => 'dashicons-calendar',
   'public' => true,
   'admin_cols' => array(
    'featured_image' => array(
     'title' => 'Image',
    ),
    'title',
    'date',
   )), array(
   # Override the base names used for labels:
   'singular' => 'Event',
   'plural' => 'Events',
   'slug' => 'events',
  ));

  register_extended_taxonomy(
   'event_category',
   strtolower(self::POST_TYPE),
   array(),
   array(
    # Override the base names used for labels:
    'singular' => 'Event Category',
    'plural' => 'Event Categories',
    'slug' => 'event_categories',
   ));

  register_extended_taxonomy(
   'event_location',
   strtolower(self::POST_TYPE),
   array(),
   array(
    # Override the base names used for labels:
    'singular' => 'Event Location',
    'plural' => 'Event Locations',
    'slug' => 'event_locations',
   ));
 }

 /**
  * Add all the ACF fields to the post type
  *
  * @return void
  */
 public static function fields()
 {
  $group_key = 'group_' . str_replace(' ', '_', self::POST_TYPE) . '_post_type';
  $group = array(
   'key' => $group_key,
   'title' => self::POST_TYPE . " Settings",
   'fields' => array(),
   'location' => array(
    array(
     array(
      'param' => 'post_type',
      'operator' => '==',
      'value' => strtolower(self::POST_TYPE),
     ),
    ),
   ),

   'menu_order' => 0,
   'position' => 'normal',
   'style' => 'default',
   'label_placement' => 'top',
   'instruction_placement' => 'label',
   'hide_on_screen' => '',
  );

  acf_add_local_field_group($group);

  acf_add_local_field(array(
   'key' => $group_key . "_start_time",
   'label' => 'Start time',
   'name' => 'start_time',
   'type' => 'date_time_picker',
   'display_format' => 'Y-m-d H:i:s',
   'return_format' => 'Y-m-d H:i:s',
   'first_day' => 0,
   'wrapper' => array(
    'width' => '50%',
   ),
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_end_time",
   'label' => 'End Time',
   'name' => 'end_time',
   'type' => 'date_time_picker',
   'display_format' => 'Y-m-d H:i:s',
   'return_format' => 'Y-m-d H:i:s',
   'first_day' => 0,
   'wrapper' => array(
    'width' => '50%',
   ),
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_recurring",
   'label' => 'Recurring Event?',
   'name' => 'frequency',
   'type' => 'select',
   'choices' => array(
    'once' => 'One Time Event',
    'weekly' => 'Weekly',
    'monthly' => 'Monthly',
    'custom' => 'Custom',
   ),
   'wrapper' => array(
    'width' => '100%',
   ),
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_recurring_days",
   'label' => 'Recurring Days',
   'name' => 'recurring_days',
   'type' => 'repeater',
   'conditional_logic' => array(
    array(
     array(
      'field' => $group_key . "_recurring",
      'operator' => '==',
      'value' => 'custom',
     ),
    ),
   ),
   'sub_fields' => array(
    array(
     'key' => $group_key . "_recurring_day",
     'label' => 'Recurring Day',
     'name' => 'recurring_day',
     'type' => 'select',
     'choices' => array(
      0 => 'Sunday',
      1 => 'Monday',
      2 => 'Tuesday',
      3 => 'Wednesday',
      4 => 'Thursday',
      5 => 'Friday',
      6 => 'Saturday',
     ),
     'return_format' => 'value',
    ),
    array(
     'key' => $group_key . "_recurring_times",
     'label' => 'Recurring Times',
     'name' => 'recurring_times',
     'type' => 'repeater',
     'sub_fields' => array(
      array(
       'key' => $group_key . "_recurring_time",
       'label' => 'Recurring Time',
       'name' => 'recurring_time',
       'type' => 'time_picker',
       'display_format' => 'g:i a',
       'return_format' => 'H:i:s',
      ),
     ),
    ),
   ),
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_address",
   'label' => 'Address',
   'name' => 'address',
   'type' => 'text',
   'default_value' => get_field('contact_address', 'options'),
   'parent' => $group_key,
  ));

  acf_add_local_field(array(
   'key' => $group_key . "_sign_up",
   'label' => 'Sign Up Form',
   'instructions' => 'This could be a url or a form shortcode.',
   'name' => 'sign_up',
   'type' => 'text',
   'parent' => $group_key,
  ));
 }

 /**
  * Create mapping routes for the post type
  *
  * @return void
  */
 public static function createRoutes()
 {
  /**
   * Add to Calender .ics download route:
   */
  \Routes::map('events/:event/add-to-calendar', function ($params) {
   $query = 'post_type=event&name=' . $params['event'];
   \Routes::load('library/actions/add-to-calendar.php', $params, $query, 200);
  });

  \Routes::map('add-to-calendar', function ($params) {
   \Routes::load('library/actions/add-to-calendar.php');
  });
 }

 /**
  * Sets up default sorting when searching for post type
  *
  * @return void
  */
 public static function setupSorting()
 {
  // Check query for events
  function query_includes_events($query)
  {
   if (is_array($query->query_vars['post_type'])) {
    if (in_array('event', $query->query_vars['post_type'])) {
     return true;
    }
   } else {
    if ($query->query_vars['post_type'] == 'event') {
     return true;
    }
   }
   return false;
  }

  // Sort events
  function ak_sort_events_by_date($query)
  {
   //if( $query->query_var['post_type'] == 'message' || $query->is_tax == 1) {
   if (query_includes_events($query) && !is_admin() && !isset($query->query_vars['orderby'])) {
    //echo $query->query_vars['post_type'];
    $query->set('orderby', 'meta_value');
    $query->set('meta_key', 'start_time');
    $query->set('order', 'ASC');
   }
  }

  // Add hook
  add_action('pre_get_posts', 'PostType\ak_sort_events_by_date', 1000);
 }

 /**- - - - - - - - - - - - - -
  * HELPER FUNCTIONS
  * - - - - - - - - - - - - - - */

 /**
  * Checks post date to automatically revert to draft when expired
  *
  * @return void
  */
 public function expire()
 {
  $now = current_datetime();
  $future = $this->expiration > $now;

  if (!$future) {
   switch ($this->frequency) {
    case 'weekly':
     $this->update_week();
     break;
    case 'monthly':
     $this->update_month();
     break;
    default:
     $current_post = get_post($this->ID, 'ARRAY_A');
     $current_post['post_status'] = 'draft';
     wp_update_post($current_post);
     break;
   }
   return true;
  } else {
   return false;
  }
 }

 /**
  * Formats a string as a date
  *
  * @param  string $date_field The date to format
  * @param  string $format The PHP date format
  * @return string The formatted date
  */
 public function create_date($date_field, $format = 'M d')
 {
  $timestamp = strtotime($date_field);
  return date($format, $timestamp);
 }

 /**
  * Formats a string as a time
  *
  * @param  string $time_field The time to format
  * @param  string $format The PHP date format
  * @return string The formatted time
  */
 public function create_time($time_field, $format = 'h:i a')
 {
  $timestamp = strtotime($time_field);
  return date($format, $timestamp);
 }

 /**
  * Update the recurrance to the next week
  *
  * @return void
  */
 public function update_week()
 {
  $date = $this->start_date_time;
  $end = $this->end_date_time;
  $now = current_datetime();

  // Keep adding weeks until we get to the future.
  while ($date < $now) {
   $date = $date->modify('+1 week');
   if ($end) {
    $end = $end->modify('+1 week');
   }
  }

  $new_date = $date->format('Y-m-d H:i:s');
  update_post_meta($this->ID, 'start_time', $new_date);
  if ($end) {
   $new_end = $end->format('Y-m-d H:i:s');
   update_post_meta($this->ID, 'end_time', $new_end);
  }
  $this->date = $date->format('Y-m-d');
 }

 /**
  * Update the recurrence to the next month
  *
  * @return void
  */
 public function update_month()
 {
  $date = $this->start_date_time;
  $now = current_datetime();

  // Calculate Ordinal
  $day = $date->format('l'); // Returns the day spelled out, like "Sunday";
  $dayth = $date->format('d'); // Returns the day of the month, like "14";

  // Determine the ordinal - Is this the "Second" Sunday?
  switch ($dayth) {
   case ($dayth < 8):
    $ordinal = 'first';
    break;
   case ($dayth < 15):
    $ordinal = 'second';
    break;
   case ($dayth < 22):
    $ordinal = 'third';
    break;
   case ($dayth < 29):
    $ordinal = 'fourth';
    break;
   case ($dayth >= 29):
    $ordinal = 'fifth';
    break;
   default:
    $ordinal = 'first';
  }

  // A modify string that finds the next such day in the next month:
  $modify_string = "{$ordinal} {$day} of next month";

  // Push forward
  if ($ordinal == 'fifth') {
   // We need an extra check for our fifths, since if PHP doesn't find a 'fifth' Sunday, it will default to the first.
   // So, if we detect that this day is not actually a fifth, we'll keep adding months until we find one.
   while ($date < $now || intval($date->format('d')) < 29) {
    $date = $date->modify($modify_string);
   }
  } else {
   // If it's not a fifth, then we just need to keep going until we're in the future.
   while ($date < $now) {
    $date = $date->modify($modify_string);
   }
  }

  $new_date = $date->format('Y-m-d H:i:s');
  update_post_meta($this->ID, 'start_time', $new_date);
  if ($end_date) {
   $diff = $this->start_date_time->diff($date);
   $new_end = $this->end_date_time->add($diff);
   update_post_meta($this->ID, 'end_time', $new_end);
  }
  $this->date = $date->format('Y-m-d');
 }

 /**
  * Update the recurrence to the next closest time.
  *
  * This will loop through all the set days/times to see when the next upcoming
  *   occurrence will happen and then sets the start date/time accordingly. If
  *   If an end date/time is set, the new end date/time will be set according to
  *   the correct duration.
  *
  * @return void
  */
 public function update_custom()
 {
  $now = current_datetime();
  $duration = false;

  if ($this->end_time) {
   $duration = $this->start_date_time->diff($this->end_date_time);
  }

  // Calculate Closest
  // $days = get_field('recurring_days');
  // error_log("days: " . print_r($days, true));
  // error_log("This: " . print_r($this, true));

  if (have_rows('recurring_days')):
   while (have_rows('recurring_days')): the_row();
    $temp_day = $now;
    $day = get_sub_field('recurring_day');

    $count = 7;
    while ($temp_day->format('w') != $day and $count > 0) {
     $temp_day = $temp_day->modify(' +1 day');
     $count = $count - 1;
    }

    if (have_rows('recurring_times')):
     while (have_rows('recurring_times')): the_row();
      $time = get_sub_field('recurring_time');

      if ($time) {
       $time_obj = explode(':', $time);
       $temp_datetime = $temp_day->setTime($time_obj[0], $time_obj[1]);
      } else {
       $temp_datetime = $temp_day->setTime(0, 0);
      }

      if ($closest) {
       $temp_diff = $temp_datetime->getTimestamp() - $now->getTimestamp();
       $closest_diff = $closest->getTimestamp() - $now->getTimestamp();
       if ($temp_diff < $closest_diff && $temp_diff > 0) {
        $closest = $temp_datetime;
       }
      } else {
       $closest = $temp_datetime;
      }
     endwhile;
    else:
     $temp_datetime = $temp_day->setTime(0, 0);

     if ($closest) {
      $temp_diff = $temp_datetime->getTimestamp() - $now->getTimestamp();
      $closest_diff = $closest->getTimestamp() - $now->getTimestamp();
      if ($temp_diff < $closest_diff && $temp_diff > 0) {
       $closest = $temp_datetime;
      }
     } else {
      $closest = $temp_datetime;
     }
    endif;
   endwhile;
  endif;

  // Set next occurance to closest
  if ($closest) {
   update_post_meta($this->ID, 'start_time', $closest->format('Y-m-d H:i:s'));
   if ($duration) {
    $new_end = $closest->add($duration);
    update_post_meta($this->ID, 'end_time', $new_end->format('Y-m-d H:i:s'));
   }
  }
 }
}

Event::setup();
