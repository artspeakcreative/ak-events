<?php
/*
Plugin Name: ArtSpeak Events
Plugin URI: http://artspeakcreative.com
Description: Event Functionality for sites that ties in with AK Membership sites
Version: 0.1.0
Author: Drew Sartorius
Author URI: http://artspeakcreative.com
License: GPL2
 */

//  PLUGIN FUNCTIONS
function ak_event_activation()
{
 //  Runwhen plugin is activated
}
register_activation_hook(__FILE__, 'ak_event_activation');

function ak_event_deactivation()
{
 // Run when plugin is deactivated
}
register_deactivation_hook(__FILE__, 'ak_event_deactivation');

function ak_event_uninstall()
{
 // Run when plugin is uninstalled
}
register_uninstall_hook(__FILE__, 'ak_event_uninstall');

// HOOKS FUNCTIONS
global $AK_EVENT_ROOT;
$AK_EVENT_ROOT = WP_PLUGIN_DIR . '/' . plugin_basename(dirname(__FILE__));
include_once "functions/index.php";

// Add Fields
// include_once "fields/settings_fields.php";
// include_once "fields/user_fields.php";

add_action('after_setup_theme', 'ak_event_twig_loc');
function ak_event_twig_loc()
{
 global $AK_EVENT_ROOT;
 $locations = Timber::$locations;
 if (is_array($locations)) {
  array_unshift($locations, $AK_EVENT_ROOT . "/templates/views");
 } else {
  $locations = array(
   $AK_EVENT_ROOT . "/templates/views",
   get_template_directory(),
   get_stylesheet_directory(),
  );
 }
 Timber::$locations = $locations;
}

// Register Scripts
function ak_event_enqueue_scripts()
{
 wp_enqueue_style('ak_event-css', plugins_url('/assets/css/events.min.css', __FILE__), array(), '', 'all');
 wp_register_script('ak_event_js', plugins_url('/assets/js/events.min.js', __FILE__), array('jquery'), '', true);
 wp_enqueue_script('ak_event_js');
//  wp_localize_script('ak_event_js', 'akEventVars', [
 //   'root' => esc_url_raw(rest_url()),
 //   'nonce' => wp_create_nonce('wp_rest'),
 //  ]);
}
add_action('wp_enqueue_scripts', 'ak_event_enqueue_scripts');
