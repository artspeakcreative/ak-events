<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */

$context = Timber::get_context();
//$post = Timber::query_post();
$post = \PostType\PostFactory::create();
$context['post'] = $post;

// CONTENT RESTRICTIONS
$hasAccess = AKRestriction::userHasAccess($post);
if ($hasAccess) {
 Timber::render(array('single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig'), $context);
} else {
 Timber::render('restricted.twig', $context);
}
