<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$context = Timber::get_context();

$term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
$tax = get_taxonomy($term->taxonomy);
$post_type = $tax->object_type[0];

$context['term'] = $term;
$context['title'] = $term->name;
$context['posts'] = Timber::get_posts();
$context['pagination'] = Timber::get_pagination();

// CONTENT RESTRICTIONS
$hasAccess = AKRestriction::userHasAccess($post);
if ($hasAccess) {
 Timber::render('event-taxonomy.twig', $context);
} else {
 Timber::render('restricted.twig', $context);
}
