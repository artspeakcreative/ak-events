<?php

function ak_event_shortcodes()
{
 global $AK_EVENT_ROOT;
 $plugin_components = $AK_EVENT_ROOT . '/templates/views/components/';
 Component::load_components($plugin_components);
}
add_action('ak_run_actions', 'ak_event_shortcodes');
