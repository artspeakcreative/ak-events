<?php

add_filter('template_include', 'ak_event_override_templates');
function ak_event_override_templates($template)
{
 global $AK_EVENT_ROOT;

 if (is_post_type_archive('event')) {
  $theme_files = array('archive-event.php', 'ak-event/archive-event.php');
  $exists_in_theme = locate_template($theme_files, false);
  if ($exists_in_theme != '') {
   return $exists_in_theme;
  } else {
   return $AK_EVENT_ROOT . '/templates/archive-event.php';
  }
 } elseif (is_singular('event')) {
  $theme_files = array('single-event.php', 'ak-event/single-event.php');
  $exists_in_theme = locate_template($theme_files, false);
  if ($exists_in_theme != '') {
   return $exists_in_theme;
  } else {
   return $AK_EVENT_ROOT . '/templates/single-event.php';
  }
 } elseif (is_tax('event_category')) {
  $theme_files = array('taxonomy-event_category.php', 'ak-event/taxonomy-event_category.php');
  $exists_in_theme = locate_template($theme_files, false);
  if ($exists_in_theme != '') {
   return $exists_in_theme;
  } else {
   return $AK_EVENT_ROOT . '/templates/taxonomy-event_category.php';
  }
 } elseif (is_tax('event_location')) {
  $theme_files = array('taxonomy-event_location.php', 'ak-event/taxonomy-event_location.php');
  $exists_in_theme = locate_template($theme_files, false);
  if ($exists_in_theme != '') {
   return $exists_in_theme;
  } else {
   return $AK_EVENT_ROOT . '/templates/taxonomy-event_location.php';
  }
 }
 return $template;
}
