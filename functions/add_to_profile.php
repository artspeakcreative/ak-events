<?php

add_action('akmt_load_profile_parts', 'ak_load_event_profile');

function ak_load_event_profile()
{
 // Tab
 new AKProfile("Events", "events", "tab");

//  Ticker
 new AKProfile("Upcoming Event", "upcoming_event", "ticker");

//  Admin
 new AKProfile("Events", "events", "admin");
}
