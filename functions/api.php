<?php

add_action('rest_api_init', function () {
 register_rest_route('akevent/v1', '/events', array(
  'methods' => 'GET',
  'callback' => 'ak_event_get_events',
 ));
 register_rest_route('akevent/v1', '/events/(?P<search>\w+)', array(
  'methods' => 'GET',
  'callback' => 'ak_event_find_events',
 ));
 register_rest_route('akevent/v1', '/event/enroll', array(
  'methods' => 'POST',
  'callback' => 'ak_event_assign_to_current_user',
 ));
 register_rest_route('akevent/v1', '/addToEvent', array(
  'methods' => 'POST',
  'callback' => 'ak_event_assign_to_user',
 ));
 register_rest_route('akevent/v1', '/removeFromEvent', array(
  'methods' => 'POST',
  'callback' => 'ak_event_remove_from_user',
 ));
 register_rest_route('akevent/v1', '/user/(?P<id>\d+)/events', array(
  'methods' => 'GET',
  'callback' => 'ak_event_get_user_events',
 ));
});

function ak_event_get_events()
{
 if (!is_user_logged_in()) {
  return new WP_Error(
   'rest_user_invalid',
   esc_html__('User not logged in.', 'ak-event'),
   array('status' => 403)
  );
 }
 if (!current_user_can('ak_admin')) {
  return new WP_Error(
   'rest_user_invalid',
   esc_html__('User not authorized.', 'ak-event'),
   array('status' => 403)
  );
 }
 $args = array(
  'post_type' => 'event',
  'numberposts' => -1,
 );
 $posts = get_posts($args);

 $response = new WP_REST_Response($posts);
 $response->set_status(200);

 return $response;
}

function ak_event_find_events($request)
{
 $args = array(
  'post_type' => 'event',
  'numberposts' => 10,
  's' => $request['search'],
 );
 $posts = get_posts($args);

 $response = new WP_REST_Response($posts);
 $response->set_status(200);

 return $response;
}

function ak_event_assign_to_user($request)
{
 if (!is_user_logged_in()) {
  return new WP_Error(
   'rest_user_invalid',
   esc_html__('User not logged in.', 'ak-event'),
   array('status' => 403)
  );
 }
 if (!current_user_can('ak_admin')) {
  return new WP_Error(
   'rest_user_invalid',
   esc_html__('User not authorized.', 'ak-event'),
   array('status' => 403)
  );
 }

 $event = $request['eventId'];
 $user = $request['userId'];

 if (!$event) {
  return new WP_Error(
   'rest_event_invalid',
   esc_html__('Missing event ID.', 'ak-event'),
   array('status' => 404)
  );
 }
 if (!$user) {
  return new WP_Error(
   'rest_user_invalid',
   esc_html__('Missing user ID.', 'ak-event'),
   array('status' => 404)
  );
 }

 $user = get_userdata($user);
 if ($user === false) {
  return new WP_Error(
   'rest_user_invalid',
   esc_html__('Invalid user ID.', 'ak-event'),
   array('status' => 404)
  );
 }

 $event = get_post($event);
 if ($event && $event->post_type == 'event') {
  $enrolled = get_field('enrolled_events', 'user_' . $user->id);
  if ($enrolled) {
   if (in_array($event->ID, $enrolled)) {
    $res = false;
   } else {
    array_push($enrolled, $event->ID);
   }
  } else {
   $enrolled = [$event->ID];
  }
  $res = update_field('enrolled_events', $enrolled, 'user_' . $user->id);
 } else {
  return new WP_Error(
   'rest_event_invalid',
   esc_html__('Invalid event ID.', 'ak-event'),
   array('status' => 404)
  );
 }
 if ($res) {
  $res = array(
   "event" => $event,
   "user" => $user,
  );
 }
 $response = new WP_REST_Response($res);
 $response->set_status(200);
 return $response;
}

function ak_event_assign_to_current_user($request)
{
 if (!is_user_logged_in()) {
  return new WP_Error(
   'rest_user_invalid',
   esc_html__('User not logged in.', 'ak-event'),
   array('status' => 403)
  );
 }
 $event = filter_var($request['eventId'], FILTER_SANITIZE_NUMBER_INT);
 $user = wp_get_current_user();

 if (!$event) {
  return new WP_Error(
   'rest_event_invalid',
   esc_html__('Missing event ID.', 'ak-event'),
   array('status' => 404)
  );
 }

 $event = get_post($event);
 if ($event && $event->post_type == 'event') {
  $enrolled = get_field('enrolled_events', 'user_' . $user->id);
  if (!$enrolled) {
   $enrolled = array();
  }
  if (in_array($event->ID, $enrolled)) {
   $res = false;
  } else {
   array_push($enrolled, $event->ID);
   $enrolled = array_values($enrolled);
   $res = update_field('enrolled_events', $enrolled, 'user_' . $user->id);
  }
 } else {
  return new WP_Error(
   'rest_event_invalid',
   esc_html__('Invalid event ID.', 'ak-event'),
   array('status' => 404)
  );
 }
 if ($res) {
  $res = array(
   "event" => $event,
   "user" => $user,
  );
 }
 $response = new WP_REST_Response($res);
 $response->set_status(200);
 return $response;
}

function ak_event_remove_from_user($request)
{
 if (!is_user_logged_in()) {
  return new WP_Error(
   'rest_user_invalid',
   esc_html__('User not logged in.', 'ak-event'),
   array('status' => 403)
  );
 }
 if (!current_user_can('ak_admin')) {
  return new WP_Error(
   'rest_user_invalid',
   esc_html__('User not authorized.', 'ak-event'),
   array('status' => 403)
  );
 }

 $event = $request['eventId'];
 $user = $request['userId'];

 if (!$event) {
  return new WP_Error(
   'rest_event_invalid',
   esc_html__('Missing event ID.', 'ak-event'),
   array('status' => 404)
  );
 }
 if (!$user) {
  return new WP_Error(
   'rest_user_invalid',
   esc_html__('Missing user ID.', 'ak-event'),
   array('status' => 404)
  );
 }

 $user = get_userdata($user);
 if ($user === false) {
  return new WP_Error(
   'rest_user_invalid',
   esc_html__('Invalid user ID.', 'ak-event'),
   array('status' => 404)
  );
 }

 $event = get_post($event);
 if ($event && $event->post_type == 'event') {
  $enrolled = get_field('enrolled_events', 'user_' . $user->id);
  if (!$enrolled) {
   $enrolled = array();
  }
  $key = array_search($event->ID, $enrolled);
  if ($key === false) {
   return new WP_Error(
    'rest_event_invalid',
    esc_html__('Event not assigned to user.', 'ak-event'),
    array('status' => 404)
   );
  } else {
   unset($enrolled[$key]);
   $enrolled = array_values($enrolled);
   $enrolled = array_values($enrolled);
   $res = update_field('enrolled_events', $enrolled, 'user_' . $user->id);
  }
 } else {
  return new WP_Error(
   'rest_event_invalid',
   esc_html__('Invalid event ID.', 'ak-event'),
   array('status' => 404)
  );
 }
 $response = new WP_REST_Response($res);
 $response->set_status(200);
 return $response;
}

function ak_event_get_user_events($request)
{
 if (!is_user_logged_in()) {
  return new WP_Error(
   'rest_user_invalid',
   esc_html__('User not logged in.', 'ak-event'),
   array('status' => 403)
  );
 }
 if (!current_user_can('ak_admin')) {
  return new WP_Error(
   'rest_user_invalid',
   esc_html__('User not authorized.', 'ak-event'),
   array('status' => 403)
  );
 }
 $events = get_user_meta($request['id'], "enrolled_events");
 $eventList = [];
 foreach ($events[0] as $event) {
  $oEvent = get_post(intval($event));
  $eventList[] = $oEvent;
 }
 $response = new WP_REST_Response($eventList);
 $response->set_status(200);
 return $response;
}
