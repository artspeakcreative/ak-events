<?php

function ak_event_add_to_context($context)
{
 $context['featured_events'] = Timber::get_posts(['post_type' => 'event', 'orderby' => 'menu_order', 'order' => 'ASC', 'event_category' => 'Featured', 'numberposts' => 4]);

 return $context;
}
add_filter('akmt_add_to_context', 'ak_event_add_to_context');
