<?php

// include_once "setup_tables.php";
include_once "register_post_type.php";
include_once "template_override.php";
include_once "add_to_context.php";
include_once "add_to_twig.php";
include_once "add_shortcodes.php";
include_once "add_to_profile.php";
include_once "add_to_sidebar.php";
include_once "api.php";
