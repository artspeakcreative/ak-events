var EVENT_ADMIN_MODAL = 'admin_events';

$(document).on('open.zf.reveal', '#' + EVENT_ADMIN_MODAL, function() {
  // Reset form
  var userLookup = $('#admin_event_user_lookup_box');
  var userInput = $('#admin_event_user');
  userInput.val("");
  userLookup.empty();

  var eventList = $('#admin_event_list');
  if(eventList.children().length == 0) {
    var modal = $('#' + EVENT_ADMIN_MODAL);
    addLoader(modal);
    $.getJSON('/wp-json/akevent/v1/events/')
      .done(function(result){
        eventList.empty();
        $.each(result, function(index, event) {
          var item = $('<li></li>');
          item.addClass("event-item").text(event.post_title).data("id", event.ID);
          eventList.append(item);
        });
      })
      .always(function() {
        deleteLoader(modal);
      });
  }
});
jQuery(document).on('fire_scripts', '#wrapper', function(){
  // Search for users
  var debounce=null;
  $('#admin_event_user').on('keyup', function(e) {
    var userLookup = $('#admin_event_user_lookup_box');
    var search = $(this).val();
    clearTimeout(debounce);
    if(search.length > 0) {
      debounce = setTimeout(function() {
        $.getJSON('/wp-json/akmembership/v1/users/' + search, function(result) {
          userLookup.empty();
          $.each(result, function(index, user) {
            var userItem = $('<li></li>');
            userItem.data("user", user.data.ID)
              .addClass('event-user')
              .text(user.data.display_name);
            userLookup.append(userItem);
          });
        });
      }, 200);
    } else {
      setTimeout(function() {
        userLookup.empty();
      }, 100);
    }
  });

  // Lookup events for user
  $('#admin_event_user_lookup_box').on('click', '.event-user', function(e) {
    e.preventDefault();
    var userId = $(this).data("user");
    var name = $(this).text();
    $('#admin_event_user_lookup_box').empty();
    $('#admin_events .event-groups').removeClass('hide');

    $('#admin_event_user').val(name);
    $('#admin_user_events').data("user", userId).empty();
    $('#admin_unassigned_events').data("user", userId).empty();

    $.getJSON('/wp-json/akevent/v1/user/' + userId + '/events', function(result) {
      var userEvents = $('#admin_user_events');
      var unassignedEvents = $('#admin_unassigned_events');
      var userEventIds = [];
      result.forEach(function(event) {
        userEventIds.push(event.ID);
      });
      $('#admin_event_list').find('li').each(function() {
        var id = $(this).data("id");
        if(userEventIds.indexOf(id) > -1) {
          var event = $(this).clone().data("id", id);
          var remove = $('<div></div>').addClass("unassign-minus");
          var icon = $('<i></i>').addClass("fas fa-minus");
          remove.append(icon);
          event.append(remove);
          userEvents.append(event);
        } else {
          var event = $(this).clone().data("id", id);
          var add = $('<div></div>').addClass("assign-plus");
          var icon = $('<i></i>').addClass("fas fa-plus");
          add.append(icon);
          event.append(add);
          unassignedEvents.append(event);
        }
      });
    });
  });

  // Assign event to user
  $('ul.user-unassigned-events').on('click', '.event-item .assign-plus', function(e) {
    var userId = $(this).parents('ul').data("user");
    var event = $(this).parent();
    var eventId = event.data("id");
    $.post('/wp-json/akevent/v1/addToEvent', {userId, eventId}, function(result) {
      console.log("Event", result.event.ID, eventId)
      console.log("User", result.user.data.ID, userId)
      if(result.event.ID === eventId && result.user.data.ID === userId) {
        $('#admin_user_events').append(event);
        var btn = event.find('div');
        btn.removeClass('assign-plus').addClass('unassign-minus');
        btn.find('i').removeClass('fa-plus').addClass('fa-minus');
      }
    });
  });
  // Unassign event from user
  $('ul.user-events').on('click', '.event-item .unassign-minus', function(e) {
    var userId = $(this).parents('ul').data("user");
    var event = $(this).parent();
    var eventId = event.data("id");
    $.post('/wp-json/akevent/v1/removeFromEvent', {userId, eventId}, function(result) {
      if(result) {
        $('#admin_unassigned_events').append(event);
        var btn = event.find('div');
        btn.removeClass('unassign-minus').addClass('assign-plus');
        btn.find('i').removeClass('fa-minus').addClass('fa-plus');
      }
    });
  })
});