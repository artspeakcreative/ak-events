<?php
/* ---------------------
 * ACF Settings Function
 * --------------------- */
function ak_course_add_user_fields()
{
 if (function_exists('acf_add_local_field_group')) {
  $group_key = 'user_fields_ak_course';
  $group = array(
   'key' => $group_key,
   'title' => "Course Settings",
   'fields' => array(),
   'location' => array(
    array(
     array(
      'param' => 'user_role',
      'operator' => '==',
      'value' => 'akviewer',
     ),
    ),
    array(
     array(
      'param' => 'user_role',
      'operator' => '==',
      'value' => 'akposter',
     ),
    ),
    array(
     array(
      'param' => 'user_role',
      'operator' => '==',
      'value' => 'akmanager',
     ),
    ),
    array(
     array(
      'param' => 'user_role',
      'operator' => '==',
      'value' => 'akadmin',
     ),
    ),
   ),
   'menu_order' => 0,
   'position' => 'normal',
   'style' => 'default',
   'label_placement' => 'top',
   'instruction_placement' => 'label',
   'hide_on_screen' => '',
  );

  acf_add_local_field_group($group);

  acf_add_local_field(array(
   'key' => $group_key . "_enrolled_courses",
   'label' => 'Enrolled Courses',
   'name' => 'enrolled_courses',
   'type' => 'post_object',
   'post_type' => array(
    0 => 'course',
   ),
   'multiple' => 1,
   'return_format' => 'id',
   'parent' => $group_key,
  ));

//   acf_add_local_field(array(
  //    'key' => $group_key . "_soundtrack_zone",
  //    'label' => 'Soundtrack Zone',
  //    'name' => 'soundtrack_zone',
  //    'type' => 'text',
  //    'parent' => $group_key,
  //   ));

//   acf_add_local_field(array(
  //    'key' => $group_key . "_carousel_zone",
  //    'label' => 'Carousel Zone',
  //    'name' => 'carousel_zone',
  //    'type' => 'text',
  //    'parent' => $group_key,
  //   ));

//   acf_add_local_field(array(
  //    'key' => $group_key . "_carousel_url",
  //    'label' => 'Carousel API URL',
  //    'name' => 'carousel_api_url',
  //    'type' => 'text',
  //    'parent' => $group_key,
  //   ));

//   acf_add_local_field(array(
  //    'key' => $group_key . "_carousel_user",
  //    'label' => 'Carousel User',
  //    'name' => 'carousel_user',
  //    'type' => 'text',
  //    'parent' => $group_key,
  //   ));

//   acf_add_local_field(array(
  //    'key' => $group_key . "_carousel_pass",
  //    'label' => 'Carousel Password',
  //    'name' => 'carousel_pass',
  //    'type' => 'text',
  //    'parent' => $group_key,
  //   ));

//   acf_add_local_field(array(
  //    'key' => $group_key . "_carousel_group",
  //    'label' => 'Carousel Group ID',
  //    'name' => 'carousel_group',
  //    'type' => 'text',
  //    'parent' => $group_key,
  //   ));

//   acf_add_local_field(array(
  //    'key' => $group_key . "_carousel_tag_id",
  //    'label' => 'Carousel tag ID',
  //    'name' => 'carousel_tag',
  //    'type' => 'text',
  //    'parent' => $group_key,
  //   ));
 }
}
add_filter('init', 'ak_course_add_user_fields');
